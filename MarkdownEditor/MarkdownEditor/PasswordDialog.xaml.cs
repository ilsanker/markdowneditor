﻿using System.Windows;

namespace MarkdownEditor
{
	/// <summary>
	/// Interaktionslogik für PasswordDialog.xaml
	/// </summary>
	public partial class PasswordDialog : Window
	{
		public PasswordDialog()
		{
			this.InitializeComponent();
		}

		public string Password { get; private set; }

		private void OkButtonClick(object sender, RoutedEventArgs e)
		{
			if (string.Equals(this.Password1.Password, this.Password2.Password))
			{
				this.Password = this.Password1.Password;
				this.DialogResult = true;
				return;
			}

			this.DialogResult = false;
		}

		private void CancelButtonClick(object sender, RoutedEventArgs e)
		{
			this.DialogResult = false;
		}
	}
}
