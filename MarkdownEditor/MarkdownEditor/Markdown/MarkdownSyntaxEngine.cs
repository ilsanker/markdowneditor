﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownEditor.Markdown
{
    class MarkdownSyntaxEngine
    {
        private bool checkForBoundingCharacters(string text, int startPos, int length, int cnt, char c)
        {
            int endOfSelection = startPos + length;
            
            // if not enough space at left or right side, returning false
            if(startPos < cnt || text.Length - cnt < endOfSelection)
            {
                return false;
            }

            char[] leftHanded = new char[cnt];
            char[] rightHanded = new char[cnt];
            for (int i = 0; i < cnt; i++)
            {
                leftHanded[i] = text[startPos - 1 - i];
                rightHanded[i] = text[endOfSelection + i];
            }
            bool result = true;
            for (int i = 0; i < cnt; i++)
            {
                result &= (leftHanded[i] == c) && (rightHanded[i] == c);
            }
            return result;
        }

        public string Bold(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if(text == null)
            {
                return null;
            }


            // if already bold, unbolding
            if (checkForBoundingCharacters(text, startPos, length, 2, '*'))
            {
                return Unbold(text, startPos, length);
            }

            // else bolding
            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "**" + text.Substring(startPos, endOfSelectionIndex - startPos) + "**";
            return leftHand + edited + rightHand;
        }

        private string Unbold(string text, int startPos, int length)
        {
            string leftHand = text.Substring(0, startPos - 2);
            string rightHand = text.Substring(startPos + length + 2, text.Length - (startPos + length + 2));
            string edited = text.Substring(startPos, length);
            return leftHand + edited + rightHand;
        }

        public string Italic(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }


            // if already bold, unbolding
            if (checkForBoundingCharacters(text, startPos, length, 1, '_'))
            {
                return Unitalic(text, startPos, length);
            }

            // else bolding
            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "_" + text.Substring(startPos, endOfSelectionIndex - startPos) + "_";
            return leftHand + edited + rightHand;
        }

        private string Unitalic(string text, int startPos, int length)
        {
            string leftHand = text.Substring(0, startPos - 1);
            string rightHand = text.Substring(startPos + length + 1, text.Length - (startPos + length + 1));
            string edited = text.Substring(startPos, length);
            return leftHand + edited + rightHand;
        }

        public string BulletList(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "* " + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string NumberedList(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "1. " + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Image(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "![Alt-Text](pfad/bild.png) " + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Cite(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "> " + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Code(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = '\t' + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Comment(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "\\" + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Email(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "<mail@adresse.tld> " + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Line(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "\n- - -\n" + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Link(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string edited = "<http://url.tld> " + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

        public string Meta(string text, int startPos, int length)
        {
            string template = "Title:␣␣␣␣MultiMarkdown - CheatSheet␣␣\n";
            template += "Date:␣␣␣␣␣2014 - 01 - 28␣␣\n";
            template += "Author:␣␣␣Mark Down, Mac Andi␣␣\n";
            template += "Keywords:␣PDF, Spickzettel, Deutsch\n\n";
            return template + text;
        }

        public string Table(string text, int startPos, int length)
        {
            // if there is no string, ignoring
            if (text == null)
            {
                return null;
            }

            int endOfSelectionIndex = startPos + length;
            string leftHand = text.Substring(0, startPos);
            string rightHand = text.Substring(endOfSelectionIndex, text.Length - endOfSelectionIndex);
            string template = "| Spalte 1 | Spalte 2 | Spalte 3 |\n";
            template += "| :------- | :------: | -------: |\n";
            template += "| Zeile 1 | Verbundene Zelle ||\n";
            template += "| Links | *Mitte * | Rechts | ";
            string edited = template + text.Substring(startPos, endOfSelectionIndex - startPos);
            return leftHand + edited + rightHand;
        }

    }
}
