﻿using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using Markdig;
using Markdig.Wpf;
using MarkdownEditor.ViewModels;

namespace MarkdownEditor
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
			this.DataContext = new MainViewModel();
			Viewer.Pipeline = new MarkdownPipelineBuilder().UseSupportedExtensions().Build();
		}

		private void OpenHyperlink(object sender, System.Windows.Input.ExecutedRoutedEventArgs e)
		{
			Process.Start(e.Parameter.ToString());
		}

		private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
		{

			var pDialog = new PrintDialog
			{
				PageRangeSelection = PageRangeSelection.AllPages,
				UserPageRangeEnabled = true
			};
			if (pDialog.ShowDialog().Value)
			{
				IDocumentPaginatorSource idpSource = this.Viewer.Document;
				pDialog.PrintDocument(idpSource.DocumentPaginator, "Markdown print ...");
			}
		}
	}
}
