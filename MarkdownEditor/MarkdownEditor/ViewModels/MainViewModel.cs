﻿using MarkdownEditor.Markdown;
using Microsoft.Win32;
using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using MarkdownEditor.Tools;

namespace MarkdownEditor.ViewModels
{
	class MainViewModel : BaseViewModel
	{
		private readonly MarkdownSyntaxEngine markdown = new MarkdownSyntaxEngine();

		private string rawText = "";
		private int rawEditorSelectionStart;
		private int rawEditorSelectionLength;

		private string filePath;

		private void _HandleBoldClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Bold(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2);
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleItalicClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Italic(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2);
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleBulletListButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.BulletList(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 1;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleNumberedListButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.NumberedList(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 14;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleImageButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Image(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 14;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleCiteButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Cite(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 1;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleCodeButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Code(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 1;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleCommentButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Comment(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 1;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleEmailButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Email(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 10;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleLineButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Line(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 4;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleLinkButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Link(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 9;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleMetaButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Meta(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = 0;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleTableButtonClick()
		{
			int lengthBefore = this.RawText.Length;
			int selStart = this.RawEditorSelectionStart + this.RawEditorSelectionLength;
			this.RawText = markdown.Table(RawText, RawEditorSelectionStart, RawEditorSelectionLength);
			int lengthDifference = this.RawText.Length - lengthBefore;
			this.RawEditorSelectionStart = selStart + (lengthDifference / 2) + 65;
			this.RawEditorSelectionLength = 0;
		}

		private void _HandleNewButtonClick()
		{
			MainWindow newWindow = new MainWindow();
			newWindow.Show();
		}

		private void _HandleOpenButtonClick()
		{
			var openFileDialog = new OpenFileDialog
			{
				Filter = "Markdown (*.md)|*.md",
				DefaultExt = "md",
				RestoreDirectory = true
			};

			if (openFileDialog.ShowDialog() == true)
			{
				//Get the path of specified file
				var filePath = openFileDialog.FileName;

				//Read the contents of the file into a stream
				this.RawText = File.ReadAllText(filePath);
				this.filePath = filePath;
			}
		}

		private void _HandleSaveButtonClick()
		{
			try
			{
				if (string.IsNullOrEmpty(this.filePath))
				{
					var saveFileDialog = new SaveFileDialog
					{
						Filter = "Markdown (*.md)|*.md",
						DefaultExt = "md",
						AddExtension = true
					};
					if (saveFileDialog.ShowDialog() == true)
					{
						File.WriteAllText(saveFileDialog.FileName, this.RawText);
					}
				}
				else
				{
					File.WriteAllText(this.filePath, this.RawText);
				}
			}
			catch (Exception)
			{
				MessageBox.Show("Failed to save file!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
			}
		}

		private void _HandleVigenereEncrypt()
		{
			var dialog = new PasswordDialog();
			if (dialog.ShowDialog() != true || string.IsNullOrEmpty(dialog.Password))
			{
				return;
			}

			this.RawText = VigenereCipher.Encrypt(this.RawText, dialog.Password);
		}

		private void _HandleVigenereDecrypt()
		{
			var dialog = new PasswordDialog();
			if (dialog.ShowDialog() != true || string.IsNullOrEmpty(dialog.Password))
			{
				return;
			}

			this.RawText = VigenereCipher.Decrypt(this.RawText, dialog.Password);
		}

		public ICommand NewButtonCommand { get; set; }
		public ICommand OpenButtonCommand { get; set; }
		public ICommand SaveButtonCommand { get; set; }
		public ICommand BoldButtonCommand { get; set; }
		public ICommand ItalicButtonCommand { get; set; }
		public ICommand BulletListButtonCommand { get; set; }
		public ICommand NumberedListButtonCommand { get; set; }
		public ICommand ImageButtonCommand { get; set; }
		public ICommand CiteButtonCommand { get; set; }
		public ICommand CodeButtonCommand { get; set; }
		public ICommand CommentButtonCommand { get; set; }
		public ICommand EmailButtonCommand { get; set; }
		public ICommand LineButtonCommand { get; set; }
		public ICommand LinkButtonCommand { get; set; }
		public ICommand MetaButtonCommand { get; set; }
		public ICommand TableButtonCommand { get; set; }
		public ICommand VigenereEncryptCommand { get; set; }
		public ICommand VigenereDecryptCommand { get; set; }

		public string RawText
		{
			get => this.rawText;
			set => this.SetPropertyChanged(ref this.rawText, value);
		}

		public int RawEditorSelectionStart
		{
			get => this.rawEditorSelectionStart;
			set => this.SetPropertyChanged(ref this.rawEditorSelectionStart, value);
		}

		public int RawEditorSelectionLength
		{
			get => this.rawEditorSelectionLength;
			set => this.SetPropertyChanged(ref this.rawEditorSelectionLength, value);
		}

		public MainViewModel()
		{
			BoldButtonCommand = new RelayCommand(o => this._HandleBoldClick());
			ItalicButtonCommand = new RelayCommand(o => this._HandleItalicClick());
			BulletListButtonCommand = new RelayCommand(o => this._HandleBulletListButtonClick());
			NumberedListButtonCommand = new RelayCommand(o => this._HandleNumberedListButtonClick());
			ImageButtonCommand = new RelayCommand(o => this._HandleImageButtonClick());
			CiteButtonCommand = new RelayCommand(o => this._HandleCiteButtonClick());
			CodeButtonCommand = new RelayCommand(o => this._HandleCodeButtonClick());
			CommentButtonCommand = new RelayCommand(o => this._HandleCommentButtonClick());
			EmailButtonCommand = new RelayCommand(o => this._HandleEmailButtonClick());
			LineButtonCommand = new RelayCommand(o => this._HandleLineButtonClick());
			LinkButtonCommand = new RelayCommand(o => this._HandleLinkButtonClick());
			MetaButtonCommand = new RelayCommand(o => this._HandleMetaButtonClick());
			TableButtonCommand = new RelayCommand(o => this._HandleTableButtonClick());
			NewButtonCommand = new RelayCommand(o => this._HandleNewButtonClick());
			OpenButtonCommand = new RelayCommand(o => this._HandleOpenButtonClick());
			SaveButtonCommand = new RelayCommand(o => this._HandleSaveButtonClick());
			VigenereEncryptCommand = new RelayCommand(o => this._HandleVigenereEncrypt());
			VigenereDecryptCommand = new RelayCommand(o => this._HandleVigenereDecrypt());
		}
	}
}
