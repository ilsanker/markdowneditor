﻿using System;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;

namespace MarkdownEditor.ViewModels
{
	/// <summary>
	/// Abstract Base View Model to be inherited by real ViewModels to implement the needed interface and add auto event triggering on updates
	/// </summary>
	public abstract class BaseViewModel : INotifyPropertyChanged
	{
		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// Sets the value of <paramref name="value"/> on the <paramref name="field"/> if the value is different.
		/// Additionally a <see cref="PropertyChanged"/> event is raised.
		/// </summary>
		/// <returns>
		/// True if the field value changed.
		/// </returns>
		protected bool SetPropertyChanged<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
		{
			if (object.Equals(field, value))
			{
				return false;
			}

			field = value;
			this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			return true;
		}

		/// <summary>
		/// Raises an <see cref="PropertyChanged"/> event for all provided properties.
		/// </summary>
		/// <example>
		/// this.RaisePropertyChanged(() => this.PROPERTY);
		/// </example>
		protected void RaisePropertyChanged<T>(Expression<Func<T>> property)
		{
			if (property?.Body is MemberExpression expression)
			{
				this.PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(expression.Member.Name));
			}
		}
	}
}
