﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace MarkdownEditor.Tools
{
	public class VigenereCipher
	{
		private const char LowerBoundUpper = 'A';
		private const char UpperBoundUpper = 'Z';
		private const char LowerBoundLower = 'a';
		private const char UpperBoundLower = 'z';
		private const int AlphabetSize = UpperBoundUpper - LowerBoundUpper + 1;

		public static string Encrypt(string plaintext, string key)
		{
			key = PrepareInput(key);

			var ciphertext = new StringBuilder();
			for (var i = 0; i < plaintext.Length; i++)
			{
				if (plaintext[i] >= LowerBoundUpper && plaintext[i] <= UpperBoundUpper)
				{
					var offset = char.ToUpper(key[i % key.Length]) - LowerBoundUpper + 1;
					var encrypted = plaintext[i] + offset;
					if (encrypted > UpperBoundUpper)
					{
						encrypted -= AlphabetSize;
					}

					ciphertext.Append((char) encrypted);
				}
				else if (plaintext[i] >= LowerBoundLower && plaintext[i] <= UpperBoundLower)
				{
					var offset = char.ToLower(key[i % key.Length]) - LowerBoundLower + 1;
					var encrypted = plaintext[i] + offset;
					if (encrypted > UpperBoundLower)
					{
						encrypted -= AlphabetSize;
					}

					ciphertext.Append((char)encrypted);
				}
				else
				{
					ciphertext.Append(plaintext[i]);
				}
			}

			return ciphertext.ToString();
		}

		public static string Decrypt(string ciphertext, string key)
		{
			key = PrepareInput(key);

			var plaintext = new StringBuilder();
			for (var i = 0; i < ciphertext.Length; i++)
			{
				if (ciphertext[i] >= LowerBoundUpper && ciphertext[i] <= UpperBoundUpper)
				{
					var offset = char.ToUpper(key[i % key.Length]) - LowerBoundUpper + 1;
					var decrypted = ciphertext[i] - offset;
					if (decrypted < LowerBoundUpper)
					{
						decrypted += AlphabetSize;
					}

					plaintext.Append((char) decrypted);
				}
				else if (ciphertext[i] >= LowerBoundLower && ciphertext[i] <= UpperBoundLower)
				{
					var offset = char.ToLower(key[i % key.Length]) - LowerBoundLower + 1;
					var decrypted = ciphertext[i] - offset;
					if (decrypted < LowerBoundLower)
					{
						decrypted += AlphabetSize;
					}

					plaintext.Append((char)decrypted);
				}
				else
				{
					plaintext.Append(ciphertext[i]);
				}
			}

			return plaintext.ToString();
		}

		//The crack method will likely only work if the ciphertext is long enough to find patterns, will not likely work on short samples
		public IEnumerable<string> Crack(string ciphertext)
		{
			var sequenceSpacings = this.IdentifySequenceSpacings(ciphertext);
			var factors = GetFactors(sequenceSpacings);

			return this.GenerateLikelyKeys(ciphertext, factors);
		}

		private static string PrepareInput(string input)
		{
			var regex = new Regex("[^A-Z]", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			return regex.Replace(input, string.Empty);
		}

		private IEnumerable<int> IdentifySequenceSpacings(string ciphertext)
		{
			var sequenceSpacings = new List<int>();
			for (var i = 0; i + 3 < ciphertext.Length; i++)
			{
				var sequence = ciphertext.Substring(i, 3);
				for (var j = i + 1; j + 3 < ciphertext.Length; j++)
				{
					if (sequence == ciphertext.Substring(j, 3))
					{
						sequenceSpacings.Add(j - i);
					}
				}
			}

			return sequenceSpacings.Distinct();
		}

		private static IEnumerable<int> GetFactors(IEnumerable<int> numbers)
		{
			var factors = new List<int>();
			foreach (var number in numbers)
			{
				var max = (int)Math.Sqrt(number);
				for (var factor = 1; factor <= max; factor++)
				{
					if (number % factor == 0)
					{
						factors.Add(factor);
						factors.Add(number / factor);
					}
				}
			}

			factors.RemoveAll(f => f == 1);

			if (!factors.Any())
			{
				return new List<int>();
			}

			var groupedFactors = factors.GroupBy(f => f).ToList();
			var mostOccurrences = groupedFactors.Max(g => g.Count());
			return groupedFactors.Where(g => g.Count() == mostOccurrences).Select(g => g.Key);
		}

		private IEnumerable<string> GenerateLikelyKeys(string ciphertext, IEnumerable<int> factors)
		{
			var keys = new List<string>();
			foreach (var factor in factors)
			{
				var substrings = GenerateSubstrings(ciphertext, factor);
				var likelySubkeys = this.GenerateLikelySubkeys(substrings);

				var possibleKeys = new List<string>();
				this.BuildKeys(possibleKeys, likelySubkeys, new char[factor], 0);

				keys.AddRange(possibleKeys);
			}

			return keys;
		}

		private static IEnumerable<string> GenerateSubstrings(string ciphertext, int factor)
		{
			for (var i = 0; i < factor; i++)
			{
				var substring = new StringBuilder();
				for (var j = i; j < ciphertext.Length; j += factor)
				{
					substring.Append(ciphertext[j]);
				}

				yield return substring.ToString();
			}
		}

		private List<List<string>> GenerateLikelySubkeys(IEnumerable<string> substrings)
		{
			var likelySubkeys = new List<List<string>>();
			foreach (var substring in substrings)
			{
				var scores = new Dictionary<string, int>();
				for (var subkey = LowerBoundUpper; subkey <= UpperBoundUpper; subkey++)
				{
					var plaintext = Decrypt(substring, subkey.ToString());
					var score = AnalyzeLetterFrequency(plaintext);

					scores.Add(subkey.ToString(), score);
				}

				var highestScore = scores.Values.Max();
				var likelySubkey = scores.Where(s => s.Value == highestScore).Select(s => s.Key).ToList();
				likelySubkeys.Add(likelySubkey);
			}

			return likelySubkeys;
		}

		private static int AnalyzeLetterFrequency(string text)
		{
			//very basic scoring method, scores for each occurrence of one of the 3 most frequent letters in english
			//from wikipedia, letters ordered by frequency from highest to lowest are: ETAOINSHRDLCUMWFGYPBVKJXQZ
			return text.Count(l => "ETA".Contains(l));
		}

		private void BuildKeys(ICollection<string> keys, IReadOnlyList<List<string>> subkeys, char[] builder, int index)
		{
			if (index == subkeys.Count)
			{
				keys.Add(new string(builder));
				return;
			}

			for (var i = 0; i < subkeys[index].Count; i++)
			{
				builder[index] = subkeys[index][i][0];
				this.BuildKeys(keys, subkeys, builder, index + 1);
			}
		}
	}
}