﻿using System;
using MarkdownEditor.Tools;
using NUnit.Framework;

namespace MarkdownEditor.Test
{
	public class VigenereCipherTest
	{
		[TestCase("aaaa", "aaaa", "bbbb")]
		[TestCase(
			"**Lorem** ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.",
			"Secret key 123!",
			"**Ogwyx** nskzg inetu xce zfjw, wzsrxyhlzl xzwnskhcyl xqllw, xdw gafg sngzpq ytwlhi ljgatq nqnnxfss zw qumtqx hl xzqnkj efayf tqlizslr xwdl, djc ilsr gtknuwmf. Fs ahjt ptr jw fwnzrtr wy uzrmt vzi inetuwx py xf jjvfr. Xwwy nqhmf cfmo fnghjllps, sr xyl stplefnl rtsflzm jrm Ogwyx hixxe xzqnk vay lrdm. Dtlpr buvmr otkhw knn flxy, hiyxdmjwmw dfcbuvunhr denwj, djc ilsr ytmnrb jccrnw wwrjzw bsyaioyy ny dfvzwd jw iiwtqx pslhl zentmdux dkfw, mpi wnde pzqtiyxs. Ly ojug yzx xy shwfxzf hl dfxsh gmt otkhwhk ye dt uwgox. Lyhl wwnst nsxx ltujuywyy, gt kju yzdnpsyu xzghwmx pxs Qrjjg nolzp iiwtq xll uxjs."
		)]
		public void EncryptTest(string plainText, string key, string expectedCipherText)
		{
			Console.WriteLine(VigenereCipher.Encrypt(plainText, key));
			Assert.That(VigenereCipher.Encrypt(plainText, key), Is.EqualTo(expectedCipherText));
		}

		[TestCase("bbbb", "aaaa", "aaaa")]
		[TestCase(
			"**Ogwyx** nskzg inetu xce zfjw, wzsrxyhlzl xzwnskhcyl xqllw, xdw gafg sngzpq ytwlhi ljgatq nqnnxfss zw qumtqx hl xzqnkj efayf tqlizslr xwdl, djc ilsr gtknuwmf. Fs ahjt ptr jw fwnzrtr wy uzrmt vzi inetuwx py xf jjvfr. Xwwy nqhmf cfmo fnghjllps, sr xyl stplefnl rtsflzm jrm Ogwyx hixxe xzqnk vay lrdm. Dtlpr buvmr otkhw knn flxy, hiyxdmjwmw dfcbuvunhr denwj, djc ilsr ytmnrb jccrnw wwrjzw bsyaioyy ny dfvzwd jw iiwtqx pslhl zentmdux dkfw, mpi wnde pzqtiyxs. Ly ojug yzx xy shwfxzf hl dfxsh gmt otkhwhk ye dt uwgox. Lyhl wwnst nsxx ltujuywyy, gt kju yzdnpsyu xzghwmx pxs Qrjjg nolzp iiwtq xll uxjs.",
			"Secret key 123!",
			"**Lorem** ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet."
		)]
		public void DecryptTest(string cipherText, string key, string expectedPlainText)
		{
			Assert.That(VigenereCipher.Decrypt(cipherText, key), Is.EqualTo(expectedPlainText));
		}
	}
}
